<?php

/**
 * @file
 * Logic for the Exclusive Value module.
 */

/**
 * Implementation of hook_form_alter().
 */
function exclusive_value_form_alter(&$form, &$form_state, $form_id) {
  // Edit the CCK field.
  _exclusive_value_cck_form_alter($form, $form_id);
}

/**
 * Add checkbox for whether a CCK field should have an exclusive value or not.
 */
function _exclusive_value_cck_form_alter(&$form, $form_id) {

  // Edit the CCK field.
  if ($form_id != 'field_ui_field_edit_form') return;

  $form['field']['exclusive_value'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Exclusive Value'),
    '#default_value' => variable_get('exclusive_value_global_' . $form['#instance']['field_name'], 0),
    '#description'   => t('If checked, this field will have only one value on a single node, the field will be empty on all other nodes.'),
  );

  $form['#submit'][] = '_exclusive_value_cck_form_submit';
}

/**
 * Save setting for whether a CCK field should have an exclusive value or not.
 */
function _exclusive_value_cck_form_submit($form, &$form_state) {
  variable_set('exclusive_value_global_' . $form_state['values']['instance']['field_name'], $form_state['values']['field']['exclusive_value']);
}

/**
 * Implements hook_node_insert().
 */
function exclusive_value_node_insert($node) {
  exclusive_value_update_nodes($node);
}

/**
 * Implements hook_node_update().
 */
function exclusive_value_node_update($node) {
  exclusive_value_update_nodes($node);
}


/**
 * Delete all values of CCK field when new value is saved.
 */
function exclusive_value_update_nodes($node) {

  // Get a list of all the CCK fields.
  $fields = field_info_fields();

  // Update CCK fields as needed
  foreach ($fields as $field) {
    $field_name = $field['field_name'];
    // Field is configured to be an exclusive value, and node field has a value.
    if (isset($field['exclusive_value']) && $field['exclusive_value'] == 1) {
      $has_value = FALSE;
      $items     = field_get_items('node', $node, $field_name);

      if (!empty($items)) {
        foreach ($items as $item) {
          if ((isset($item['value']) && !empty($item['value'])) || (isset($item['tid']) && !empty($item['tid']))) {
            $has_value = TRUE;
          }
        }
      }

      if ($has_value) {

        $table  = _field_sql_storage_tablename($field);

        if ($field['type'] == "taxonomy_term_reference") {
          $column = _field_sql_storage_columnname($field_name, 'tid');
        }
        else {
          $column = _field_sql_storage_columnname($field_name, 'value');
        }

        // Get ID of nodes that are not the current node, and that have a value in the field configured to be an exclusive value.
        $query = db_select($table, 'f');
        $query
          ->fields('f', array('entity_id', 'revision_id'))
          ->condition('f.entity_id', $node->nid, '<>')
          ->condition('f.' . $column, '', '!=')
          ->isNotNull('f.' . $column);

        $result      = $query->execute();
        $value_count = $query->countQuery()->execute()->fetchField();

        switch ($value_count) {
          case 0:
            return;
          case 1:
            $old_node = $result->fetchObject();

            // Clear value on the old node.
            db_update($table)
              ->fields(array($column => 0)) // @todo - look up default value and use that.
              ->condition('entity_id', $old_node->entity_id, '=')
              ->execute();

            // Clear cache for node that has been updated.
            db_delete('cache_field')
              ->condition('cid', "field:node:$old_node->entity_id")
              ->execute();

            break;
          default:
            // If field has been in use before configured to be exclusive value, it may have more than one value to be deleted.

            // Clear value on the old nodes.
            db_update($table)
              ->fields(array($column => 0)) // @todo - look up default value and use that.
              ->condition('entity_id', $node->nid, '<>')
              ->execute();

            // Clear cache for nodes that have been updated.
            $query = db_delete('cache_field');
            $query->execute();
            // @todo - is there a way to do an INNER JOIN on a delete so we can delete only cache rows for this particular field?
            //  db_query("DELETE c FROM {cache_content} c INNER JOIN {%s} f ON c.cid = CONCAT('content:', f.nid, ':', f.vid)", $db_info['table']);
            break;
        }
      }
    }
  }
}






